import { Component,ViewChild } from '@angular/core';
import { Section } from './item';
import {MatAccordion} from '@angular/material/expansion';


@Component({
  selector: 'app-root' ,
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent{
  title = 'code';
  hidden = false;
  opened = false;
  note = 7;
  showRunner = false;

//  topics=[]
  
@ViewChild(MatAccordion) accordion: MatAccordion;
  

  

  

folders: Section[] = [
  {
    name: 'Photos',
    updated: new Date('1/1/16'),
  },
  {
    name: 'Recipes',
    updated: new Date('1/17/16'),
  },
  {
    name: 'Work',
    updated: new Date('1/28/16'),
  }
];

  action() {
    this.hidden = !this.hidden;
  }
  load() {
    this.showRunner = true;
    setTimeout(() => {
      this.showRunner = false
    }, 3000);
  }

  loge(state) {
    console.log(state)
  }
}
